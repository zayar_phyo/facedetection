package android.eman.cz.tflitedemo

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import org.tensorflow.lite.Interpreter
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader
import java.nio.FloatBuffer
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import java.util.*


/**
 * Classifies images with Tensorflow Lite
 */
class ImageClassifier(activity: Activity) {
    val TAG = this.javaClass.name

    val modelPath  = "model.tflite"
    val labelPath = "labels.txt"
    val imageSizeX = 150
    val imageSizeY = 150

    val imgDataBuffer: FloatBuffer
    val labelList: List<String>

    var tflite: Interpreter? = null

    init {
        tflite = Interpreter(loadModelFile(activity))
        labelList = loadLabelList(activity)
        imgDataBuffer = FloatBuffer.allocate(imageSizeX * imageSizeY)
        Log.d(TAG, "Created Tensorflow Lite Image Classifier")
    }

    /**
     * Receives an image as bitmap for classification.
     */
    fun classifyFrame(bitmap: Bitmap): FloatArray {
        if (tflite == null) {
            Log.e(TAG, "Image classifier has not been initialized")
        }
//        applyTransformation(bitmap)

        val features = convertImageToFloatArray(bitmap)
        return runInference(features)
    }

    private fun convertImageToFloatArray(image: Bitmap): Array<Array<FloatArray>>? {
        val modelInputDim = imageSizeX
        val imageArray = Array(modelInputDim) { Array(modelInputDim) { FloatArray(3) } }
        for (x in 0 until modelInputDim) {
            for (y in 0 until modelInputDim) {
                val R = Color.red(image.getPixel(x, y))
                val G = Color.green(image.getPixel(x, y))
                val B = Color.blue(image.getPixel(x, y))
                val grayscalePixel = (0.3 * R + 0.59 * G + 0.11 * B) / 255
                imageArray[x][y][0] = grayscalePixel.toFloat()
            }
        }
        return imageArray
    }

    /**
     * Here you can apply any transformations to your image, before classification.
     */
    private fun applyTransformation(bitmap: Bitmap) {
        imgDataBuffer.rewind()
        val intValues = IntArray(imageSizeX * imageSizeY)
        bitmap.getPixels(intValues, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)

        var pixel = 0
        while (pixel < imageSizeX*imageSizeY) {
            transformPixelValue(intValues[pixel++])
        }
    }

    /**
     * Evaluate model with data from image.
     */
    private fun runInference(features: Array<Array<FloatArray>>?): FloatArray {
//        val input = Array(1) { Array(150) { Array(150) { FloatArray(3) } } }
        val input = Array (1) { _ -> features }
//        val input = Array (1) { Array(150) { Array(150) { _ -> features } } }
        val output = Array(1) { _ -> FloatArray(labelList.size)}
        tflite?.run(input, output)
        return output[0]
    }


    /**
     * Decompose pixel into colors and transform pixel value.
     */
    private fun transformPixelValue(rgb: Int) {
        val red = rgb shr 16 and 0xFF
        val green = rgb shr 8 and 0xFF
        val blue = rgb and 0xFF

        val grayScale = (red + green + blue) / 3
        val normalized = grayScale / 255f

        imgDataBuffer.put(normalized)

    }

    /**
     * Load labels list from assets.
     */
    @Throws(IOException::class)
    private fun loadLabelList(activity: Activity): List<String> {
        val labelList = ArrayList<String>()
        val reader = BufferedReader(InputStreamReader(activity.assets.open(labelPath)))
        var line = reader.readLine()
        while (line != null) {
            labelList.add(line)
            line = reader.readLine()
        }
        reader.close()
        return labelList
    }

    /**
     * Load model file from assets.
     */
    @Throws(IOException::class)
    private fun loadModelFile(activity: Activity): MappedByteBuffer {
        val fileDescriptor = activity.assets.openFd(modelPath)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    /**
     * De-initialize TFLite interpreter.
     */
    fun close() {
        tflite!!.close()
        tflite = null
    }
}
